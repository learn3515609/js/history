push.addEventListener('click', e => {
  history.pushState({button: 'push'}, 'push', '/push')
})

replace.addEventListener('click', e => {
  history.replaceState({button: 'replace'}, 'replace', '/replace')
})

state.addEventListener('click', e => {
  info.value = JSON.stringify(history.state);
})

back.addEventListener('click', e => {
  history.back();
})

forward.addEventListener('click', e => {
  history.forward();
})